clc;
clear all;
%--- INIZIALIZZAZIONE ---

PSI0 = 50;

xa = 10; ya = 7;
xb = 99; yb = 100;
xc = 89; yc = 100;
xd = 10; yd = 39.5;    

xg = 51.5; yg = 54.5; xf = 64; yf = 59.5;
rg = 47.5; rf = 20;
Q = 100;
dr = 15;
dz = 3;

CF=sqrt((xc-xf)^2+(yc-yf)^2);
xp1=(xf*(CF^2-rf^2)+xc*rf^2+sqrt((xf*(CF^2-rf^2)+xc*rf^2)^2-(CF^2)*((xf^2-rf^2)*(CF^2-rf^2)+xc^2*rf^2)))/(CF^2);
xp2=(xf*(CF^2-rf^2)+xc*rf^2-sqrt((xf*(CF^2-rf^2)+xc*rf^2)^2-(CF^2)*((xf^2-rf^2)*(CF^2-rf^2)+xc^2*rf^2)))/(CF^2);
if xp1>xf
    xp=xp1;
elseif xp2>xf
    xp=xp2;
end
yp1=yf+sqrt(rf^2-(xp-xf)^2);
yp2=yf-sqrt(rf^2-(xp-xf)^2);
if yp1<yf
    yp=yp1;
elseif yp2<yf
    yp=yp2;
end
 xm=xg;ym=ya;
xn=xg+rg;yn=yg;
xo=xf;yo=yd;

%disegno le circonferenze

figure(1); hold on;
x0=xm;
y0=ym;
b=0.01;
x1=xg+rg*cos(3*pi/2+b);
y1=yg+rg*sin(3*pi/2+b);
xci=[xm;x1];yci=[ym;y1];
for i=(3*pi/2):b:(2*pi-2*b)
    x2=xg+2*cos(b)*(x1-xg)-(x0-xg);
    xci=[xci;x2];
    x0=x1;
    x1=x2;
end
for i=(3*pi/2):b:(2*pi-2*b)
    y2=yg+2*cos(b)*(y1-yg)-(y0-yg);
    yci=[yci;y2];
    y0=y1;
    y1=y2;
end
xci=[xci;xn];yci=[yci;yn];
plot(xci,yci,'r');hold on;

%traccio la circonferenza superiore
%innanzitutto trovo l'angolo di arrivo della circonferenza conoscendo il
%punto node e il centro di circonferenza

thetap=2*pi-atan((yf-yp)/(xp-xf));
iter=3*pi/2+b;
x0=xo;y0=yo;
x1=xf+rf*cos(3*pi/2+b);
y1=yf+rf*sin(3*pi/2+b);
xcs=[xo;x1];ycs=[yo;y1];
while iter<thetap
    x2=xf+2*cos(b)*(x1-xf)-(x0-xf);
    xcs=[xcs;x2];
    x0=x1;
    x1=x2;
    iter=iter+b;
end
iter=3*pi/2+b;
while iter<thetap
    y2=yf+2*cos(b)*(y1-yf)-(y0-yf);
    ycs=[ycs;y2];
    y0=y1;
    y1=y2;
    iter=iter+b;
end
xcs=[xcs;xp];ycs=[ycs;yp];
plot(xcs,ycs,'r');
hold off;

beta = atan((yf-yp)/(xf-xp));


%--- DISCRETIZZAZIONE DEL CAMPO FLUIDO ---

%Retta OM
mom = (ya - yd) / (xg - xf);
qom = (xg*yd - xf*ya)/(xg - xf);

%Retta FG
mfg = (yg - yf) / (xg - xf);
qfg = (xg*yf - xf*yg)/(xg - xf);

%Retta PN
mpn = (yg - yp) / (xb - xp);
qpn = (xb*yp - xp*yg)/(xb - xp);

mcC = (yc - yp) / (xc - xp);
qcC = (xc*yp - xp*yc)/(xc - xp);

% y di partenza delle linee di flusso dal tratto AD
y_in = linspace(ya, yd, dr+2);
y_in(1) = []; y_in(dr+1) = []; 

% x di arrivo delle linee di flusso nel tratto BC
x_out = linspace(xb,xc, dr+2);
x_out(1) = []; x_out(dr+1) = [];

centri = zeros(dr, 2);
raggi = zeros(dr, 1);
iso_end = zeros(dr, 2);

%Bordo interno
s = zeros(dr+2, 3);
s(1,1) = norm([xf,yd] - [xd,yd]);
s(1,2) = s(1,1) + rf * (pi/2 - abs(beta));
s(1,3) = s(1,2) + norm([xc,yc] - [xp,yp]);

k = 2;
% isolinee
for i = dr:-1:1
    % coordinata x del centro della circonferenza
    xc1 = (y_in(i) - qom) / mom;
    % coordinata y del centro della circonferenza
    yc1 = mfg * xc1 + qfg;    
    % raggio circonferenza
    r = yc1 - y_in(i);

    % intersezione circonferenza con retta PN
    x = (-(mpn * (qpn - yc1) - xc1) + sqrt((mpn * (qpn - yc1) - xc1)^2 - (1 + mpn^2) * (xc1^2 + (qpn - yc1)^2 - r^2))) / (1 + mpn^2);
    y = mpn * x + qpn;
    theta = atan2(y - yc1, x - xc1);

    iso_end(i,:) = [x, y];
    centri(i,:) = [xc1 yc1];
    raggi(i) = r;

    pc = draw_circle( 3*pi/2,0.01,xc1,yc1,r,2*pi + theta );
    isolinee{i} = [xa, y_in(i); pc; x_out(i), yb];

    s(k,1) = xc1 - xd;
    s(k,2) = s(k,1) + r * abs(pi/2 + theta);
    s(k,3) = s(k,2) + sqrt((x_out(i) - x)^2 + (yb - y)^2);

    k = k + 1;
end

%Bordo esterno
iso_end = [[xb, yg]; iso_end; [xp,yp]];
s(dr+2,1) = norm([xa,ya] - [xg,ya]);
s(dr+2,2) = s(k,1) + rg * pi / 2;
s(dr+2,3) = s(k,2) + norm([xb, yg] - [xb,yb]);

%--- SUDDIVISIONE NODI ---

% lunghezza del tratto CD
len_iso = s(1,3);
n = floor(len_iso / dz);

Discr = cell(n,1);
y_in = fliplr(y_in);
y_in = [yd, y_in, ya];
x_out = fliplr(x_out);
x_out = [xc, x_out, xb];
raggi = flipud(raggi);
raggi = [rf; raggi; rg];
centri = flipud(centri);
centri = [[xf,yf]; centri; [xg,yg]];
iso_end = flipud(iso_end);

%incremento sull'ascissa curvilinea
ds = dz * ones(1, dr+2);

%tratto AD
rt = xa * ones(length(y_in), 1);
Q_perc = zeros(length(y_in), 1);
for i = 1 : length(y_in)
    Q_perc(i) = (y_in(i)^2 - ya^2) / (yd^2 - ya^2) * 100; %inizializzo portata
end

%Matrice totale nodi
Nodes = zeros(n*dr + 2*n + 2*length(y_in), 3);

c = dr+2;
Nodes(1:c,:) = [rt, y_in', Q_perc];
c = c + 1;

%Discretizzazione punti interni
for i = 1:n
    temp = zeros(dr+2,3);
    node = [];

    for j = 1:(dr+2)
        k = 1;
        cond = true;
        while (k <= 3 && cond)
            if (ds(j) <= s(j,k))
                cond = false;
            end
            k = k + 1;
        end
        k = k - 1;

        [node, s_glob] = initNode(k, j, ds(j), node, xa, centri, raggi,iso_end,yc,y_in,s,mcC,x_out,dr);
        ds(j) = s_glob;
        
        Nodes(c,:) = node;
        temp(j,:) = Nodes(c,:);
        c = c + 1;
    end
    Discr{i} = temp;
    ds = ds(:) + dz;
end

%tratto BC
zt = yb * ones(length(x_out), 1);
Q_perc = zeros(length(x_out), 1);
for i = 1 : length(y_in)
    Q_perc(i) = (x_out(i)^2 - xb^2) / (xc^2 - xb^2) * 100;
    Nodes(c,:) = [x_out(i), zt(i), Q_perc(i)];
    c = c + 1;
end

%Riordino nodi
for i = 1:(n+2)
    y_temp=Nodes(i*(dr+3):1:(i+1)*(dr+2),2);
    Q_temp=Nodes(i*(dr+3):1:(i+1)*(dr+2),3);
    
    y_temp=fliplr(y_temp);
    Q_temp=fliplr(Q_temp);
    
    Nodes(i*(dr+3):1:(i+1)*(dr+2),2)=y_temp;
    Nodes(i*(dr+3):1:(i+1)*(dr+2),3)=Q_temp;
    
end

%--- INIZIALIZZAZIONE ELEMENTI TRIANGOLARI ---

ne = (dr + 1) * (n + 1); %numero elementi
tri_temp = cell(2*ne,1);

column = 1;
m = 1;
k = 1;
j = 1;
h = 1;

for i = 1 : ne
    rettangolo = [j,j+1,j+1+dr+2,j+dr+2];
    
    tri_temp{h} = [rettangolo(1), rettangolo(2), rettangolo(4)];
    tri_temp{h+1} = [rettangolo(2), rettangolo(3), rettangolo(4)];
    h = h+2;

    if (j < column * (dr + 1) + (column-1))
        j = j + 1;
        k = k + 1;
    else
        j = column * (dr + 2) + 1;
        column = column + 1; %passo alla colonna successiva
        m = m + 1;
        k = 1;
    end
end

% ordino opportunamente gli elementi triangolari
elementi_triangolari = cell(2*ne,1);
j = 1;
column = 1;
c = 0;
for i = 1 : ne 
    elementi_triangolari{j} = tri_temp{2*i-1};
    elementi_triangolari{j + dr + 1} = tri_temp{2*i};

    j = j + 1;

    if (mod(i,dr+1) == 0)
        column = column + 1;
        c = c + 2;
        j = c * (dr + 1) + 1;
    end
end

%--- DIAGONALI ---

%trovo i nodi delle diagonali di tutti gli elementi rettangolari

nodi_temp = Nodes;
column = 1;
nodi_diag = cell(n+1+dr, 1);

for i = 1:n+1
    index = [];

    for j = 1:dr+2
        m = (j - 1) * (dr + 2) + column;
        if (m <= size(Nodes, 1))
            index = [index, m];
        end
        column = column + 1;
    end
    nodi_diag{i} = index;
end

for i = 1:dr
    index = [];
    column = 1;
    for j = 1 : dr + 2 - i
        m = i + (j - 1) * (dr + 2) + column;
        index = [index, m];
        column = column + 1;
    end
    nodi_diag{n+1+i} = index;
end

%--- ELEMENTI FINITI ---

%calcolo i parametri: alfa, beta, gamma, Area e baricentroicentro per ogni elemento triangolare
[ alfa,beta,gamma,area,baricentro ] = coeff_calculator( Nodes,elementi_triangolari,ne );

np = size(Nodes, 1); % numero nodi della discretizzazione
Mat_Rig = zeros(np, np); % Matrice di Rigidezza
b_noto = zeros(np, 1); % termine noto

for i =1:2*ne
    for j = 1:3
        r = elementi_triangolari{i}(j);
        for k = 1:3
            c = elementi_triangolari{i}(k);
            Mat_Rig(r,c) = Mat_Rig(r,c) + (beta(i,j) * beta(i,k) + gamma(i,j) * gamma(i,k)) * Q / (4 * baricentro(i, 2) * area(i));
        end
    end
end

% assegno psi 0 nel tratto AB
for i = 1:n+2
    k = (i-1) * (dr+2) + 1;
    for j = 1:np
        if(not(j == k))
            Mat_Rig(k, j) = 0;
            Mat_Rig(j, k) = 0;
        end
    end
    b_noto(k) = 0;
end

% assegno psi 100 nel tratto CD
for i = 1:n+2
    k = i * (dr+2);
    for j = 1 : np
        if(not(j == k))
            b_noto(j) = b_noto(j) - Mat_Rig(j, k) * 100;
        else
            b_noto(k) = 100 * Mat_Rig(k,k);
        end
    end
    for j = 1 : np
        if(not(j == k))
            Mat_Rig(k, j) = 0;
            Mat_Rig(j, k) = 0;
        end
    end
end

%Risolvo la Matrice di Rigidezza per trovare le psi
Psi = linsolve(Mat_Rig, b_noto);
Nodes(:,3) = Psi;

Psi_1 = zeros(n+2,2); %sopra 45�
Psi_2 = zeros(n+2,2); %sotto 45�
cont1 = 1;
cont2 = 1;

for i = 1:np-1
    if ((mod(i,dr) ~= 0) && (Nodes(i,3) <= (100-PSI0)) && (Nodes(i+1,3) >= (100-PSI0)))
        nodo_prec = Nodes(i,:);
        nodo_after = Nodes(i+1,:);
        z = nodo_prec(1) + ((100-PSI0) - nodo_prec(3)) / (nodo_after(3) - nodo_prec(3)) * (nodo_after(1) - nodo_prec(1));
        r = nodo_prec(2) + ((100-PSI0) - nodo_prec(3)) / (nodo_after(3) - nodo_prec(3)) * (nodo_after(2) - nodo_prec(2));
        
        if (abs((nodo_prec(2) - nodo_after(2)) / (nodo_prec(1) - nodo_after(1))) >= 1)
            Psi_1(cont1,:) = [z, r];
            cont1 = cont1 + 1;
        else
            if (cont2 == 1)
                Psi_2(cont2,:) = Psi_1(cont1-1,:);
                cont2 = cont2 + 1;
            end
            Psi_2(cont2,:) = [z, r];
            cont2 = cont2 + 1;
        end
    end
end

Psi_1(cont1:end,:)=[];
Psi_2(cont2:end,:)=[];

x = Psi_1(1,1) : .5 : Psi_1(size(Psi_1,1), 1);
y = spline(Psi_1(:,1), Psi_1(:,2), x);
Psi = [x', y'];
x = Psi_2(1,2) : .5 : Psi_2(size(Psi_2,1), 2);
y = spline(Psi_2(:,2), Psi_2(:,1), x);
Psi = [Psi; y', x'];

%--- GRAFICI ---

figure(1); hold on;

title('Elementi Finiti')
xlabel('z');
ylabel('r');
axis([xa-10 xb+10 ya-7 yb+10]);

plot(xa,ya,'ko'); text(xa-3,ya-3,'A');
plot(xb,yb,'ko'); text(xb+3,yb+3,'B');
plot(xc,yc,'ko'); text(xc-5,yc+3,'C');
plot(xd,yd,'ko'); text(xd-3,yd+5,'D');

%traccio ad e cd

xad=[xa,xd];yad=[ya,yd];xbc=[xb,xc];ybc=[yb,yc];
plot(xad,yad,'r');
plot(xbc,ybc,'r');

xam=[xa,xm];yam=[ya,ym];
xnb=[xn,xb];ynb=[yn,yb];
xod=[xo,xd];yod=[yo,yd];
plot(xam,yam,'r');
plot(xnb,ynb,'r');
plot(xod,yod,'r');

plot([xc,xp],[yc,yp],'r'); 
text(xp-4,yp+3,'P');
plot(xp,yp,'ko');

M=[xm,ym];plot(xm,ym,'ko');text(xm,ym-3,'M');
N=[xn,yn];plot(xn,yn,'ko');text(xn+3,yn,'N');
O=[xo,yo];plot(xo,yo,'ko');text(xo-2,yo+5,'O');

text(xg-4,yg+3,'G');
plot(xg,yg,'k*');
text(xf-4,yf+3,'F');
plot(xf,yf,'k*');

% disegno le linee equicorrente per la discretizzazione
for i = 1 : length(isolinee)
    line(isolinee{i}(:,1), isolinee{i}(:,2), 'Color', [0.8 0.8 0.8] , 'LineSmoothing','on');
end

% disegno le linee equipotenziali per la discretizzazione
for i = 1 : length(Discr)
    line(Discr{i}(:,1), Discr{i}(:,2), 'Color', [0.8 0.8 0.8] , 'LineSmoothing','on');
end

% disegno le linee nodi_diag (quelle che tagliano i quadrati in triangoli)
for i = 1 : size(nodi_diag, 1)
    index = nodi_diag{i};
    vertici_el = nodi_temp(index,:);
    line(vertici_el(:,1), vertici_el(:,2), 'Color', [0.8 0.8 0.8], 'LineSmoothing','on');
end

% disegno la linea di equicorrente cercata.
line(Psi(:,1), Psi(:,2), 'Color', 'b', 'LineWidth', 2);

hold off;