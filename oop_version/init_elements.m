function [ M, elements ] = init_elements( M, dz, dr )
%INIT_ELEMENTS Crea e inizializza gli elementi nella sezione della
%turbomacchina

    fprintf(' --- Inizializzazione Elementi ---');
    startingTime = cputime;
    
    elements = {};
    ne = 0;
    nn = 0;
    
    for i = 1:1:length(M)
        for j = 1:1:length(M{i})
            
            %---CONTROLLO ELEMENTO SUPERIORE---
            if(isa(M{i}{j+1},'Nodo') && isa(M{i+1}{j},'Nodo'))
                ne = ne +1;
                M{i}{j}.addElement(ne);
                elements{ne} = Elemento([M{i}{j}.getZ(),M{i}{j}.getR()],[M{i}{j}.getZ()+dz,M{i}{j}.getR()],[M{i}{j}.getZ(),M{i}{j}.getR()+dr]);
      
            end
        end
    end
    
    endTime = cputime - startingTime;
    fprintf('\n --- Inizializzazione elementi completata in %f secondi --- \n', endTime);
end