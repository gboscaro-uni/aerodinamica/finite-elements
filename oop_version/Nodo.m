classdef Nodo < handle
    %NODO Rappresenta un nodo e indica tutti gli elementi e nodi che lo
    %influenzano
    
    properties (SetAccess = 'private', GetAccess = 'public')
        z; r; %coordinate del nodo
        elements; %elementi che influenzano il nodo
        edge;
    end
    
    methods (Access = 'public')
        %COSTRUTTORE
        function obj = Nodo(z,r,brd)
            
            obj.z = z; obj.r = r;      
            obj.edge = brd;
        end
        
        %GETTERs
        function r = getZ(obj)
            r = obj.z;
        end
        function r = getR(obj)
            r = obj.r;
        end 
        function r = isEdge(obj)
            r = obj.edge;
        end
        function addElement(obj,el)
            index = find(obj.elements == el, 1);
            if(isempty(index))
                obj.elements = [obj.elements, el];
            end
        end
    end
    
    methods (Access = 'private')
    end
    
end

