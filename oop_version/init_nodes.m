function [ M, counter ] = init_nodes( dz,dr,Pa,Pb,Pc,Pd,Pp,Pg,Pf )
%INIT_NODES Crea i nodi della sezione turbomacchina

    fprintf(' --- Inizializzazione Nodi ---');
    startingTime = cputime;
    counter = 0;

    xg = Pg(1); yg = Pg(2); rg = Pg(3); xf = Pf(1); yf = Pf(2); rf = Pf(3);
    fcircle_big_y = @(x) yg-sqrt(rg^2-(x-xg)^2);
    fcircle_small_y = @(x) yf-sqrt(rf^2-(x-xf)^2);
    fcircle_big_x = @(y) xg+sqrt(rg^2-(y-yg)^2);
    fcircle_small_x = @(y) xf+sqrt(rf^2-(y-yf)^2);  
    fline_y = @(x) (Pc(2)-Pp(2))*(x-Pp(1))/(Pc(1)-Pp(1)) + Pp(2);
    fline_x = @(y) (Pc(1)-Pp(1))*(y-Pp(2))/(Pc(2)-Pp(2)) + Pp(1);

    l = ceil((Pb(1)-Pa(1))/dz); %numero di divisioni in dz
    M = cell(l,1); %cell array contentente le colonne di nodi
   
    x = Pa(1); %x iniziale
    
    i = 1;
    while i <= l
        %---LIMITI---
        %asse x - contorno inferiore
        if(x <= Pg(1)) %segmento AM
            ymin = Pa(2);
            ymincontorno = ymin;
        else %semicerchio MN
            ymincontorno = feval(fcircle_big_y,x);
            nn = floor(abs(ymincontorno-Pa(2))/dr)+1;            
            ymin = Pa(2) + nn*dr;             
        end
        %asse x - contorno superiore
        if (x <= Pf(1)) %segmento DO
            ymax = Pd(2);
        elseif (x > Pf(1) && x <= Pp(1)) %semicerchio OP
            ymax = feval(fcircle_small_y,x);
        elseif (x > Pp(1) && x < Pc(1)) %semiretta PC
            ymax = feval(fline_y,x);
        else %segmento CB
            ymax = Pc(2);
        end
        
        %---CREAZIONE NODI---
        s = length(M{i});
        n = ceil((ymax-ymin)/dr) + s;        
        y = ymin;         
            
        for j = (s+1):1:n
            M{i}{j} = Nodo(x,y,false);            
            y = y + dr;   
        end 
        
        if(ymax > y - dr)
            %aggiungo in coda il nodo del bordo superiore
            M{i}{n+1} = Nodo(x,ymax,true);
        end
        if(ymincontorno < ymin)
            %aggiungo in testa il nodo del bordo inferiore
            M{i} = [ {Nodo(x,ymincontorno,true)}, M{i}];
        end
        
        %---INTORNO SINISTRO--- 
        if (x >= Pf(1) && x <= (Pc(1) + dz))
            k = length(M{i});              
            nodi_contorno = {};
            while k >= 1
                if( not(M{i}{k}.isEdge()) )
                    rr = M{i}{k}.getR(); 
                    if(rr >= Pp(2))
                        %semiretta PC
                        zz = feval(fline_x,M{i}{k}.getR());
                        d = abs(x - zz);
                        if(d < dz)
                            %aggiungo nodo nella colonna precedente
                            nodi_contorno = [{Nodo(zz,rr,true)} , nodi_contorno];                        
                        else
                            break;
                        end

                    elseif(rr >= Pd(2) && rr <= Pp(2))
                        %semicerchio OP
                        zz = feval(fcircle_small_x,M{i}{k}.getR());
                        d = abs(x - zz);
                        if(d < dz)
                            %aggiungo nodo nella colonna precedente
                            nodi_contorno = [{Nodo(zz,rr,true)} , nodi_contorno];
                        else
                            break;
                        end
                    end
                end
                k = k - 1;
            end
            M{i-1} = [ M{i-1}, nodi_contorno];
        end
        
        %---INTORNO DESTRO---        
        if(x >= Pg(1)) 
            n = length(M{i});
            k = 1;
            nodi_contorno = {};
            while k <= n
                if( not(M{i}{k}.isEdge()) )
                    rr = M{i}{k}.getR();
                    if(rr <= Pg(2))
                        %semicerchio MN                
                        zz = feval(fcircle_big_x,rr);
                        d = abs(zz - x);
                        if(d < dz)
                            %aggiungo nodo nella colonna successiva
                            nodi_contorno = [nodi_contorno, {Nodo(zz,rr,true)}];
                        end
                    else
                        %segmento NB
                        d = abs(Pb(1) - x);
                        if(d < dz)                        
                            %aggiungo nodo nella colonna successiva
                            nodi_contorno = [nodi_contorno, {Nodo(Pb(1),rr,true)}];     
                        end
                    end
                end
                k = k + 1;
            end
            M{i+1} = nodi_contorno;
        end
        counter = counter + length(M{i});
        x = x + dz;
        i = i + 1;
    end
    
    %aggiungo i nodi in B e C
    m = length(M);
    M{m} = [M{m}, {Nodo(Pb(1),Pb(2),true)}];
    p = ceil((Pc(1)-Pa(1))/dz);        
    M{p} = [M{p}, {Nodo(Pc(1),Pc(2),true)}];
    counter = counter +2;
    
    endTime = cputime - startingTime;
    fprintf('\n --- Inizializzazione Nodi completata in %f secondi --- \n', endTime);
end