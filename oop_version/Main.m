clear all;
%coordinate punti notevoli
xa=10; ya=7;
xb=99; yb=100;
xc=89; yc=100;
xd=10; yd=39.5;
xg=51.5; yg=54.5; rg=47.5;
xf=64; yf=59.5; rf=20;

dz=2.3; dr=2.3;
tol=1e-6;
percentuale_flusso = 0.5;

turbomacchina = Turbomacchina([xa,ya],[xb,yb],[xc,yc],[xd,yd],[xg,yg,rg],[xf,yf,rf]);
turbomacchina.init();
turbomacchina.init_elements(dz,dr);

%debug
turbomacchina.show_nodes();