classdef Elemento < handle
   
    properties (SetAccess = 'private', GetAccess = 'private')
        %Triangolo ABC (ijk)
        zi; ri; zj; rj; zk; rk; %coordinate globali dei vertici
        pi; pj; pk; %valori di psi nei nodi
        D; %2*Area
        %Coefficenti
        alphai; alphaj; alphak;
        betai; betaj; betak;
        gammai; gammaj; gammak;
        zb; rb; %coordinate baricentro
        Ni; Nj; Nk; %funzioni di forma
        n; %numero dell'elemento
    end
        
    methods (Access = public)
        %COSTRUTTORE
        function obj = Elemento(i,j,k)
            
            obj.zi = i(1); obj.ri = i(2);
            obj.zj = j(1); obj.rj = j(2);
            obj.zk = k(1); obj.rk = k(2);
            
            obj.calc_coeff();    
            obj.calc_center();
            obj.def_fun();
        end
        %GETTERs
        function r = getZi(obj)
            r = obj.zi;
        end        
        function r = getZj(obj)
            r = obj.zj;
        end       
        function r = getZk(obj)
            r = obj.zk;
        end       
        function r = getRi(obj)
            r = obj.ri;
        end      
        function r = getRj(obj)
            r = obj.rj;
        end      
        function r = getRk(obj)
            r = obj.rk;
        end      
        function r = getPi(obj)
            r = obj.pi;
        end  
        function r = getPj(obj)
            r = obj.pj;
        end  
        function r = getPk(obj)
            r = obj.pk;
        end  
        function r = getD(obj)
            r = obj.D;
        end      
        function r = getZb(obj)
            r = obj.zb;
        end       
        function r = getRb(obj)
            r = obj.rb;
        end  
        %Struttura dati
        function r = next(obj)
            r = obj.next;
        end
        function r = prev(obj)
            r = obj.prev;
        end
        function r = upper(obj)
            r = obj.up;
        end
        function r = lower(obj)
            r = obj.down;
        end
        %SETTERs    
        
    end
    
    methods (Access = private)
        function calc_coeff(obj)
            %Calcola i coefficenti geometrici dell'elemento
            
            %Coefficenti alpha
            obj.alphai = obj.zj*obj.rk - obj.zk*obj.rj;
            obj.alphaj = obj.zk*obj.ri - obj.zi*obj.rk;
            obj.alphak = obj.zi*obj.rj - obj.zk*obj.ri;
            %Coefficenti beta
            obj.betai = obj.rj - obj.rk;
            obj.betaj = obj.rk - obj.ri;
            obj.betak = obj.ri - obj.rj;
            %Coefficenti gamma
            obj.gammai = obj.zk - obj.zj;
            obj.gammaj = obj.zi - obj.zk;
            obj.gammak = obj.zj - obj.zi;
            %2*Area
            obj.D = obj.alphai + obj.alphaj + obj.alphak
        end
        function calc_center(obj)            
            %Calcola le coordinate del baricentro del triangolo
            
            obj.zb = (obj.gammaj*(abs(obj.D)/3 - obj.alphai) - obj.gammai*(abs(obj.D)/3 - obj.alphaj)) / (obj.betai*obj.gammaj - obj.betaj*obj.gammai);
            obj.rb = (obj.betai*(abs(obj.D)/3 - obj.alphaj) - obj.betaj*(abs(obj.D)/3 - obj.alphai)) / (obj.betaj*obj.gammai - obj.betai*obj.gammaj);
        end
        function def_fun(obj)
            %Definisce le funzioni di forma per i nodi del triangolo
            
            obj.Ni = (obj.alphai + obj.betai*obj.zi + obj.gammai*obj.ri)/abs(obj.D);
            obj.Nj = (obj.alphaj + obj.betaj*obj.zj + obj.gammaj*obj.rj)/abs(obj.D);
            obj.Nk = (obj.alphak + obj.betak*obj.zk + obj.gammak*obj.rk)/abs(obj.D);
        end
    end

end