classdef Turbomacchina < handle

   properties (SetAccess = 'private', GetAccess = 'private')
      Pa; Pb; Pc; Pd;
      Pp; Pg; Pf;
      M; dr; dz;
      counter;
   end   
   
   properties (SetAccess = 'public', GetAccess = 'public')    
   end
    
    methods (Access = public)
        function obj = Turbomacchina(pa,pb,pc,pd,pg,pf)
            
            obj.Pa = pa;
            obj.Pb = pb;
            obj.Pc = pc;
            obj.Pd = pd;
            obj.Pg = pg;
            obj.Pf = pf;
            
            obj.counter = 0;
           
            obj.solve_eqn();
            
        end
        
        function init(obj)
            figure(1); 
            hold on;
            %limiti assi
            xmax=obj.Pb(1)+10; ymax=obj.Pb(2)+10;
            xmin=obj.Pa(1)-10; ymin=obj.Pa(2)-10;
            axis([xmin xmax ymin ymax]); 
            %plot
            obj.draw_label();
            obj.draw_segment();
            obj.draw_circle();
            hold off;
        end
        function obj = init_elements(obj,dr,dz)
            obj.dr = dr; obj.dz = dz;
            [obj.M, obj.counter] = init_nodes( obj.dz,obj.dr,obj.Pa,obj.Pb,obj.Pc,obj.Pd,obj.Pp,obj.Pg,obj.Pf );
            [obj.M] = init_elements( obj.M, obj.dz, obj.dr );
        end
        function obj = calc_psi(obj)
            create_matrix();
            solve_matrix();
        end
        function obj = interpolate(obj,psi)
            interp(psi);
        end
        %funzioni di debugging
        function show_nodes(obj)
            figure(1); 
            hold on;
            for i=1:1:length(obj.M)
                for j=1:1:length(obj.M{i})
                    plot(obj.M{i}{j}.getZ(),obj.M{i}{j}.getR(),'.k');
                end
            end
            hold off;
        end
    end
    
    methods (Access = private)
        function draw_label(obj)
            %nomi punti
            plot(obj.Pa(1), obj.Pa(2), 'ko'); text(obj.Pa(1)-5,obj.Pa(2)-3,'A');
            plot(obj.Pb(1), obj.Pb(2), 'ko'); text(obj.Pb(1)+3,obj.Pb(2)+3,'B');
            plot(obj.Pc(1), obj.Pc(2), 'ko'); text(obj.Pc(1)-5,obj.Pc(2)+3,'C');
            plot(obj.Pd(1), obj.Pd(2), 'ko'); text(obj.Pd(1)-5,obj.Pd(2)+3,'D');
            plot(obj.Pf(1), obj.Pf(2)-obj.Pf(3), 'ko'); text(obj.Pf(1)-5,obj.Pf(2)-obj.Pf(3)+3,'O');

            plot(obj.Pg(1), obj.Pg(2)-obj.Pg(3), 'ko'); text(obj.Pg(1)+3,obj.Pg(2)-obj.Pg(3)-3,'M');
            plot(obj.Pg(1)+obj.Pg(3), obj.Pg(2), 'ko'); text(obj.Pg(1)+obj.Pg(3)+3,obj.Pg(2)-3,'N');
            
            plot(obj.Pp(1), obj.Pp(2), 'ko'); text(obj.Pp(1)-5,obj.Pp(2)+3,'P');
            
            plot(obj.Pg(1), obj.Pg(2), 'k*'); text(obj.Pg(1)-5,obj.Pg(2)+3,'G');
            plot(obj.Pf(1), obj.Pf(2), 'k*'); text(obj.Pf(1)-5,obj.Pf(2)+3,'F');
        end
        function draw_segment(obj)
            %segmenti
            plot([obj.Pa(1),obj.Pd(1)],[obj.Pa(2),obj.Pd(2)],'r-');
            plot([obj.Pb(1),obj.Pc(1)],[obj.Pb(2),obj.Pc(2)],'r-');
            plot([obj.Pa(1),obj.Pg(1)],[obj.Pa(2),obj.Pg(2)-obj.Pg(3)],'r-');
            plot([obj.Pb(1),obj.Pg(1)+obj.Pg(3)],[obj.Pb(2),obj.Pg(2)],'r-');
            plot([obj.Pd(1),obj.Pf(1)],[obj.Pd(2),obj.Pf(2)-obj.Pf(3)],'r-');
            plot([obj.Pc(1),obj.Pp(1)],[obj.Pc(2),obj.Pp(2)],'r-');
        end
        function draw_circle(obj)
            % circonferenze
            a=3*pi/2; b=0.0001;
            PG=draw_circle(a,b,obj.Pg(1),obj.Pg(2),obj.Pg(3),2*pi);
            plot(PG(:,1),PG(:,2),'r-');

            a=3*pi/2; b=0.0001; t=a+atan((obj.Pp(1)-obj.Pf(1))/(obj.Pf(2)-obj.Pp(2)));
            PF=draw_circle(a,b,obj.Pf(1),obj.Pf(2),obj.Pf(3),t);
            plot(PF(:,1),PF(:,2),'r-');
        end
        function obj = solve_eqn(obj)
            CF=sqrt((obj.Pc(1)-obj.Pf(1))^2+(obj.Pc(2)-obj.Pf(2))^2); %segmento CF
            delta=(obj.Pf(1)*(CF^2-obj.Pf(3)^2)+obj.Pc(1)*obj.Pf(3)^2)^2-(CF^2)*((obj.Pf(1)^2-obj.Pf(3)^2)*(CF^2-obj.Pf(3)^2)+obj.Pc(1)^2*obj.Pf(3)^2); %delta equazione
            %soluzioni x
            xp1=(obj.Pf(1)*(CF^2-obj.Pf(3)^2)+obj.Pc(1)*obj.Pf(3)^2)/(CF^2)+(sqrt(delta))/(CF^2);
            xp2=(obj.Pf(1)*(CF^2-obj.Pf(3)^2)+obj.Pc(1)*obj.Pf(3)^2)/(CF^2)-(sqrt(delta))/(CF^2);
            if xp1>obj.Pf(1)
                obj.Pp(1)=xp1;
            else
                obj.Pp(2)=xp2;
            end
            %soluzioni y
            yp1=obj.Pf(2)+sqrt(obj.Pf(3)^2-(obj.Pp(1)-obj.Pf(1))^2);
            yp2=obj.Pf(2)-sqrt(obj.Pf(3)^2-(obj.Pp(1)-obj.Pf(1))^2);
            if yp1<obj.Pf(2)
                obj.Pp(2)=yp1;    
            else
                obj.Pp(2)=yp2;    
            end
        end    
    end
    
end