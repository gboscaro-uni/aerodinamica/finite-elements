function [nodo, s_glob] = initNode(k, j, ds, p, xa, center, radius,inters,yc,y_in,s,mcC,x_out,dr)
        switch k
            case 1, % tratto orizzontale
                x = xa + ds;
                y = y_in(j);
                nodo = [x y 0]; %coordinate nodo a destra, inizializzo portata a 0
                s_glob = ds;
            case 2, % arco di circonferenza
                if (j == 1) %circonferenza piccola
                    a = ds - s(j,k-1); 
                    theta = a / radius(j);
                    x = center(j, 1) + radius(j) * cos(3*pi/2 + theta);
                    y = center(j, 2) + radius(j) * sin(3*pi/2 + theta);
                    nodo = [x y 0];
                    s_glob = ds;
                else
                    p1 = p;
                    p2 = center(j, :);
                    theta = atan2(p2(2) - p1(2), p2(1) - p1(1)) + pi;
                    x = p2(1) + radius(j) * cos(theta);
                    y = p2(2) + radius(j) * sin(theta);
                    nodo = [x y 0];
                    s_glob = s(j, k-1) + radius(j) * abs(theta - 3/2*pi);
                end
            case 3, % tratto obliquo
                if (j == 1)
                    a = ds - s(j,k-1);
                    xr = a / sqrt((1 + mcC^2));
                    yr = mcC * xr;
                    x = inters(j, 1) + xr;
                    y = inters(j, 2) + yr;
                    nodo = [x y 0];
                    s_glob = ds;
                else                
                    A = inters(j,:);
                    B = [x_out(j), yc];
                    C = [p(1), p(2)];

                    a = norm(A - B);
                    b = norm(B - C);
                    c_ = norm(C - A);
                    thB = acos((a^2 + b^2 - c_^2) / (2 * a * b));
                    h = b * sin(thB);
                    thC = pi / 2 - thB;
                    thCB = atan2(B(2) - C(2), B(1) - C(1));
                    th = -(thC - thCB);
                    nodo = C + h * [cos(th), sin(th)];
                    s_glob = s(j, k-1) + norm(A - [nodo(1), nodo(2)]);
                    nodo = [nodo, 0];
                end
            otherwise,
                nodo = [0 0 0];
                s_glob = ds;
        end
        
    switch j
        case 1,
            nodo(3) = 100;
        case (dr+2),
            nodo(3) = 0;
        otherwise,
            nodo(3) = 28;
    end
end