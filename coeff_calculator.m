function [ alfa,beta,gamma,area,baricentro ] = coeff_calculator( Nodes,elementi_triangolari,ne )
    
    alfa = zeros(2*ne, 3);
    beta = zeros(2*ne, 3);
    gamma = zeros(2*ne, 3);
    area = zeros(2*ne, 1);
    baricentro = zeros(2*ne, 2);

    for i = 1 : 2*ne

        vertici_el = Nodes(elementi_triangolari{i}, :);
        alfa(i, 1) = vertici_el(2,1) * vertici_el(3,2) - vertici_el(3,1) * vertici_el(2,2);
        alfa(i, 2) = vertici_el(3,1) * vertici_el(1,2) - vertici_el(1,1) * vertici_el(3,2);
        alfa(i, 3) = vertici_el(1,1) * vertici_el(2,2) - vertici_el(2,1) * vertici_el(1,2);
        beta(i, 1) = vertici_el(2,2) - vertici_el(3,2);
        beta(i, 2) = vertici_el(3,2) - vertici_el(1,2);
        beta(i, 3) = vertici_el(1,2) - vertici_el(2,2);
        gamma(i, 1) = vertici_el(3,1) - vertici_el(2,1);
        gamma(i, 2) = vertici_el(1,1) - vertici_el(3,1);
        gamma(i, 3) = vertici_el(2,1) - vertici_el(1,1);

        area(i) = abs((alfa(i, 1) + alfa(i, 2) + alfa(i, 3)) / 2);
        baricentro(i, 1) = (vertici_el(1,1) + vertici_el(2,1) + vertici_el(3,1)) / 3;
        baricentro(i, 2) = (vertici_el(1,2) + vertici_el(2,2) + vertici_el(3,2)) / 3;
    end
end